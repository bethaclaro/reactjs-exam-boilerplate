## Welcome

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It includes [SASS](https://sass-lang.com) and components by [AntD](https://ant.design).


## Getting Started

New here? Great. Clone this repository, create a new branch (you may name it whatever you like) and ask your interviewer (probably ME) for further instructions.

