import React from 'react';
import { Button, Input } from 'antd';

const InputPane = ({ handleClick }) => {
  return (
    <div className='leftPane'>
      <Input placeholder='Type a string...' />
      <Button type='primary' onClick={handleClick}>Go</Button>
    </div>
  );
};

export default InputPane;