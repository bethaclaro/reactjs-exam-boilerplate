import React from 'react';


const ListDisplay = ({ data }) => {
  return (
    <div>
      {data && data.map(item => {
        return <strong>{item}</strong>
      })}
    </div>
  );
};

export default ListDisplay;