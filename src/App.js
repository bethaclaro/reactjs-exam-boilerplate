import React from 'react';
// import { Button, Input } from 'antd';
import './assets/app.scss';
import ListDisplay from './components/ListDisplay';
import InputPane from './components/InputPane';


function App() {

  const handleClick = (e) => {
    console.log(e);
  };

  return (
    <div className="App">

      <div className='container'>

        <InputPane handleClick={handleClick} />

        <div className='rightPane'>
          <ListDisplay />
        </div>
      </div>

    </div>
  );
}

export default App;
